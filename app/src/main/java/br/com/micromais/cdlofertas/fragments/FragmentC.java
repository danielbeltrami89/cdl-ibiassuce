package br.com.micromais.cdlofertas.fragments;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonArrayRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import br.com.micromais.cdlofertas.OfertasParc;
import br.com.micromais.cdlofertas.R;
import br.com.micromais.cdlofertas.adapter.ListaAdapterLojas;
import br.com.micromais.cdlofertas.app.AppController;
import br.com.micromais.cdlofertas.model.Ofertas;

/**
 * Created by Dan on 24/01/2017.
 */

public class FragmentC extends Fragment implements SwipeRefreshLayout.OnRefreshListener  {

    private static final String TAG = FragmentC.class.getSimpleName();

    // Movies json url
    private static final String url1 = "http://www.cdlibiassuce.com.br/sistema/adm/ofertas/json/getlojas.php";
    private String url_comeco = "http://www.cdlibiassuce.com.br/sistema/adm/ofertas/";
    private List<Ofertas> ofertasList = new ArrayList<>();
    private ListView listView;
    private ListaAdapterLojas adapter;
    private SwipeRefreshLayout swipeLayout;

    @Nullable
    @Override
    public View onCreateView (LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState){
        View rootView = inflater.inflate(R.layout.fragment_c, container, false);

        swipeLayout = (SwipeRefreshLayout) rootView.findViewById(R.id.swipe_container);
        swipeLayout.setOnRefreshListener(this);
        adapter =new ListaAdapterLojas(getActivity(), ofertasList);
        listView = (ListView) rootView.findViewById(R.id.lista_c_lojas);
        listView.setAdapter(adapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView adapterView, View view, int i, long l) {
                TextView v = (TextView) view.findViewById(R.id.url);
                String url = v.getText().toString();
                Intent intent = new Intent(getContext(), OfertasParc.class);
                intent.putExtra("link", url);
                startActivity(intent);
            }
        });


        swipeLayout.post(new Runnable() {
                             @Override
                             public void run() {
                                 swipeLayout.setRefreshing(true);
                                 atualizaLista();
                             }
                         }
        );

        return rootView;
    }

    private void atualizaLista () {
        swipeLayout.setRefreshing(true);

        JsonArrayRequest movieReq = new JsonArrayRequest(url1,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        Log.d(TAG, response.toString());

                        // Parsing json
                        for (int i = 0; i < response.length(); i++) {
                            try {
                                JSONObject obj = response.getJSONObject(i);
                                Ofertas ofertas = new Ofertas();


                                ofertas.setTitle(obj.getString("nome"));
                                ofertas.setYear(obj.getString("descricao"));
                                String urltemp = obj.getString("url_imagem");
                                String temp = urltemp.replace("../","");
                                ofertas.setThumbnailUrl(url_comeco+temp); //img
                                ofertas.setCategoria(obj.getString("categoria"));
                                ofertas.setUrl(obj.getString("id"));
                                ofertasList.add(ofertas);


                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                            swipeLayout.setRefreshing(false);
                        }
                        adapter.notifyDataSetChanged();
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d(TAG, "Error: " + error.getMessage());
                swipeLayout.setRefreshing(false);
            }
        });

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(movieReq);

    }

    public void onRefresh(){
        ofertasList.clear();
        atualizaLista();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

    }

}
