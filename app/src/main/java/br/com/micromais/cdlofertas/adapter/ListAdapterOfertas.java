package br.com.micromais.cdlofertas.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.NetworkImageView;

import java.util.List;

import br.com.micromais.cdlofertas.*;
import br.com.micromais.cdlofertas.app.AppController;
import br.com.micromais.cdlofertas.model.Ofertas;

public class ListAdapterOfertas extends BaseAdapter {
	private Activity activity;
	private LayoutInflater inflater;
	private List<Ofertas> ofertasItems;
    private String urls;

	TextView descont;

	ImageLoader imageLoader = AppController.getInstance().getImageLoader();

	public ListAdapterOfertas(Activity activity, List<Ofertas> ofertasItems) {
		this.activity = activity;
		this.ofertasItems = ofertasItems;
	}

	@Override
	public int getCount() {
		return ofertasItems.size();
	}

	@Override
	public Object getItem(int location) {
		return ofertasItems.get(location);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {

		if (inflater == null)
			inflater = (LayoutInflater) activity
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		if (convertView == null)
			convertView = inflater.inflate(R.layout.list_row, null);

		if (imageLoader == null)
			imageLoader = AppController.getInstance().getImageLoader();
		NetworkImageView thumbNail = (NetworkImageView) convertView
				.findViewById(R.id.thumbnail);

		//NetworkImageView thumbLoja = (NetworkImageView) convertView.findViewById(R.id.thumbloja);

		TextView title = (TextView) convertView.findViewById(R.id.title);
		TextView url = (TextView) convertView.findViewById(R.id.url);
		TextView year = (TextView) convertView.findViewById(R.id.preco);
		TextView nomeLoja = (TextView) convertView.findViewById(R.id.nomeloja);
		descont = (TextView) convertView.findViewById(R.id.desconto);

		// getting movie data for the row
		Ofertas m = ofertasItems.get(position);

				// thumbnail image
		thumbNail.setImageUrl(m.getThumbnailUrl(), imageLoader);
		//thumbLoja.setImageUrl(m.getLojaThumbUrl(),imageLoader);
		
		// title
		url.setVisibility(View.INVISIBLE);

		url.setText(m.getUrl());
		title.setText(m.getTitle());
		year.setText(m.getYear());
        nomeLoja.setText(m.getNomeloja());

		if(m.getDesconto().equalsIgnoreCase("0")){
			descont.setVisibility(View.INVISIBLE);
		}else {
			descont.setText(m.getDesconto());
			descont.setVisibility(View.VISIBLE);
		}

		return convertView;
	}


}