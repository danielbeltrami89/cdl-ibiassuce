package br.com.micromais.cdlofertas;

import android.app.ProgressDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.util.Log;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.NetworkImageView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import br.com.micromais.cdlofertas.app.AppController;

public class OfertaAct extends AppCompatActivity {
    private static final String TAG = OfertaAct.class.getSimpleName();

    private static final String url1 = "http://www.cdlibiassuce.com.br/sistema/adm/ofertas/json/getoferta.php?id=";
    private String url_comeco = "http://www.cdlibiassuce.com.br/sistema/adm/ofertas/";

    private NetworkImageView mNetworkImageView;
    private ImageLoader mImageLoader;
    private String url, titulo, preco, links, urls, conteudo, validade;
    private ProgressDialog carregar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_oferta);

        links = getIntent().getStringExtra("link");
        urls = url1 + links;

        if(getSupportActionBar() != null){
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setTitle("Oferta");
        }

        carregar = new ProgressDialog(this);
        carregar.setMessage("Carregando...");
        carregar.show();

        JsonArrayRequest movieReq = new JsonArrayRequest(urls,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        Log.d(TAG, response.toString());
                        hideCarregar();

                        for (int i = 0; i < response.length(); i++) {

                            try {
                                JSONObject obj = response.getJSONObject(i);
                                titulo = obj.getString("titulo");
                                String urltemp = obj.getString("url_imagem");
                                url = url_comeco + urltemp.replace("../", "");
                                preco = obj.getString("valor");
                                validade = obj.getString("validade");
                                conteudo = obj.getString("descricao");


                                mNetworkImageView = (NetworkImageView) findViewById(R.id.thumbnail);
                                TextView ttitulo = (TextView) findViewById(R.id.titulo_oferta);
                                TextView tdescri = (TextView) findViewById(R.id.descri_oferta);
                                TextView tpreco = (TextView) findViewById(R.id.preco);
                                TextView tvalidade = (TextView) findViewById(R.id.validade);

                                mNetworkImageView.setImageUrl(url, mImageLoader);
                                ttitulo.setText(titulo);
                                tpreco.setText(preco);
                                tdescri.setText(Html.fromHtml(conteudo));
                                tvalidade.setText(validade);


                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    }

                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d(TAG, "Error: " + error.getMessage());
                hideCarregar();

            }
        });


        AppController.getInstance().addToRequestQueue(movieReq);

        mImageLoader = AppController.getInstance().getImageLoader();

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        hideCarregar();
    }

    private void hideCarregar() {
        if (carregar != null) {
            carregar.dismiss();
            carregar = null;
        }
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }
}