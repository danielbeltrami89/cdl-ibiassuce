package br.com.micromais.cdlofertas.servicos;

import android.util.Log;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;

/**
 * Created by Dan on 03/05/2017.
 */

public class InstanceIDFirebase extends FirebaseInstanceIdService {

    public static final String TAG = "OFERTAS";

    @Override
    public void onTokenRefresh() {
        super.onTokenRefresh();

        String token = FirebaseInstanceId.getInstance().getToken();
        Log.d(TAG,"Token"+token);

    }
}
