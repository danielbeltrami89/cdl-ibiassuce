package br.com.micromais.cdlofertas.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.NetworkImageView;

import java.util.List;

import br.com.micromais.cdlofertas.R;
import br.com.micromais.cdlofertas.app.AppController;
import br.com.micromais.cdlofertas.model.Ofertas;

public class ListAdapterCategorias extends BaseAdapter {
	private Activity activity;
	private LayoutInflater inflater;
	private List<Ofertas> ofertasItems;
    private String urls;

	ImageLoader imageLoader = AppController.getInstance().getImageLoader();

	public ListAdapterCategorias(Activity activity, List<Ofertas> ofertasItems) {
		this.activity = activity;
		this.ofertasItems = ofertasItems;
	}

	@Override
	public int getCount() {
		return ofertasItems.size();
	}

	@Override
	public Object getItem(int location) {
		return ofertasItems.get(location);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {

		if (inflater == null)
			inflater = (LayoutInflater) activity
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		if (convertView == null)
			convertView = inflater.inflate(R.layout.lista_categ, null);

		if (imageLoader == null)
			imageLoader = AppController.getInstance().getImageLoader();
		NetworkImageView thumbNail = (NetworkImageView) convertView
				.findViewById(R.id.thumbnail);

		TextView title = (TextView) convertView.findViewById(R.id.tvCategoria);
		TextView url = (TextView) convertView.findViewById(R.id.url);

		Ofertas m = ofertasItems.get(position);

		thumbNail.setImageUrl(m.getThumbnailUrl(), imageLoader);

		url.setVisibility(View.INVISIBLE);

		url.setText(m.getUrl());
		title.setText(m.getTitle());

		return convertView;
	}


}