package br.com.micromais.cdlofertas.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonArrayRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import br.com.micromais.cdlofertas.OfertasCat;
import br.com.micromais.cdlofertas.R;
import br.com.micromais.cdlofertas.adapter.ListAdapterCategorias;
import br.com.micromais.cdlofertas.app.AppController;
import br.com.micromais.cdlofertas.model.Ofertas;

/**
 * Created by Dan on 24/01/2017.
 */

public class FragmentA extends Fragment {

    ListView listView;
    private List<Ofertas> ofertasList = new ArrayList<Ofertas>();
    private ListAdapterCategorias adapter;

    private static final String TAG = FragmentA.class.getSimpleName();
    private String url_comeco = "http://www.cdlibiassuce.com.br/sistema/adm/ofertas/";
    private static final String url1 = "http://www.cdlibiassuce.com.br/sistema/adm/ofertas/json/getcategoria.php";

    @Nullable
    @Override
    public View onCreateView (LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState){

        View v= inflater.inflate(R.layout.fragment_a, container, false);


        adapter = new ListAdapterCategorias(getActivity(), ofertasList);
        listView = (ListView) v.findViewById(R.id.list_a);
        listView.setAdapter(adapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                TextView v = (TextView) view.findViewById(R.id.url);
                TextView v1 = (TextView) view.findViewById(R.id.tvCategoria);
                String url = v.getText().toString();
                String nome = v1.getText().toString();

                Intent intent = new Intent(getContext(), OfertasCat.class);
                Bundle extras = new Bundle();
                extras.putString("LINK", url);
                extras.putString("NOME", nome);
                intent.putExtras(extras);
                startActivity(intent);
            }
        });

        JsonArrayRequest movieReq = new JsonArrayRequest(url1,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        Log.d(TAG, response.toString());

                        for (int i = 0; i < response.length(); i++) {
                            try {
                                JSONObject obj = response.getJSONObject(i);
                                Ofertas ofertas = new Ofertas();

                                ofertas.setTitle(obj.getString("nome_categoria")); //name
                                ofertas.setUrl(obj.getString("id_categoria")); //id

                                String urltemp = obj.getString("url_imagem");
                                String temp = urltemp.replace("../","");

                                ofertas.setThumbnailUrl(url_comeco+temp); //img
                                ofertasList.add(ofertas);


                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                        adapter.notifyDataSetChanged();
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d(TAG, "Error: " + error.getMessage());

            }
        });


        AppController.getInstance().addToRequestQueue(movieReq);


        return v;
    }

}