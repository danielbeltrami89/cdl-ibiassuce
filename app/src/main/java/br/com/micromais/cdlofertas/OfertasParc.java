package br.com.micromais.cdlofertas;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonArrayRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import br.com.micromais.cdlofertas.adapter.ListAdapterOfertas;
import br.com.micromais.cdlofertas.app.AppController;
import br.com.micromais.cdlofertas.model.Ofertas;

public class OfertasParc extends AppCompatActivity implements SwipeRefreshLayout.OnRefreshListener {

    private List<Ofertas> ofertasList = new ArrayList<Ofertas>();
    private ListView listView;
    private ListAdapterOfertas adapter;
    private SwipeRefreshLayout swipeLayout;
    private String link, links;

    private static final String TAG = OfertasParc.class.getSimpleName();
    private final String url1 = "http://www.cdlibiassuce.com.br/sistema/adm/ofertas/json/getofertasloja.php?id=";
    private String url_comeco = "http://www.cdlibiassuce.com.br/sistema/adm/ofertas/";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_b);

        link = getIntent().getStringExtra("link");
        Log.d("Link", "" + link);
        links = url1 + link;

        if(getSupportActionBar() != null){
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setTitle("Oferta");
        }

        swipeLayout = (SwipeRefreshLayout) findViewById(R.id.swipe_container);
        swipeLayout.setOnRefreshListener(this);

        listView = (ListView) findViewById(R.id.lista_b_oferta);
        adapter = new ListAdapterOfertas(this, ofertasList);
        listView.setAdapter(adapter);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView adapterView, View view, int i, long l) {
                TextView v = (TextView) view.findViewById(R.id.url);
                String url = v.getText().toString();
                Log.e("Lits", url);
                Intent intent = new Intent(getApplicationContext(), OfertaAct.class);
                intent.putExtra("link", url);
                startActivity(intent);
            }
        });

        swipeLayout.post(new Runnable() {
                             @Override
                             public void run() {
                                 swipeLayout.setRefreshing(true);
                                 atualizaLista();
                             }
                         }
        );
    }

    public void onRefresh() {
        ofertasList.clear();
        atualizaLista();
    }

    private void atualizaLista() {
        swipeLayout.setRefreshing(true);

        JsonArrayRequest movieReq = new JsonArrayRequest(links,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        Log.d(TAG, response.toString() + links);
                        if (response.length() > 0) {

                            for (int i = 0; i < response.length(); i++) {
                                try {
                                    JSONObject obj = response.getJSONObject(i);
                                    Ofertas ofertas = new Ofertas();

                                    ofertas.setTitle(obj.getString("titulo_oferta"));
                                    ofertas.setNomeloja(obj.getString("nomeloja"));
                                    ofertas.setUrl(obj.getString("id_oferta"));
                                    ofertas.setYear(obj.getString("valor"));
                                    String urltemp = obj.getString("url_imagem");
                                    String temp = urltemp.replace("../","");
                                    ofertas.setThumbnailUrl(url_comeco+temp); //img
                                    ofertas.setDesconto(obj.getString("desconto"));
                                    ofertasList.add(ofertas);

                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                                swipeLayout.setRefreshing(false);
                            }
                        } else {
                            Toast.makeText(getApplicationContext(), "Esse parceiro não tem ofertas", Toast.LENGTH_SHORT).show();
                            onBackPressed();
                        }
                        adapter.notifyDataSetChanged();
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d(TAG, "Error: " + error.getMessage());
                swipeLayout.setRefreshing(false);
            }
        });


        AppController.getInstance().addToRequestQueue(movieReq);

    }

    @Override
    public void onDestroy() {
        super.onDestroy();

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }
}
