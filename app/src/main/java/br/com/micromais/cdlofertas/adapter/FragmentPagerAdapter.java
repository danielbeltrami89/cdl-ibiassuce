package br.com.micromais.cdlofertas.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import br.com.micromais.cdlofertas.fragments.FragmentA;
import br.com.micromais.cdlofertas.fragments.FragmentB;
import br.com.micromais.cdlofertas.fragments.FragmentC;
import br.com.micromais.cdlofertas.fragments.FragmentD;

/**
 * Created by Dan on 24/01/2017.
 */

public class FragmentPagerAdapter extends FragmentStatePagerAdapter {

    private String[] mTabTitles;

    public FragmentPagerAdapter(FragmentManager fm, String[] mTabTitles) {
        super(fm);
        this.mTabTitles= mTabTitles;
    }

    @Override
    public Fragment getItem(int position) {

        switch (position){
            case 0:
                return new FragmentA();
            case 1:
                return new FragmentB();
            case 2:
                return new FragmentC();
            case 3:
                return new FragmentD();
            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return this.mTabTitles.length;
    }

    @Override
    public CharSequence getPageTitle (int position){
        return  this.mTabTitles[position];
    }

}


