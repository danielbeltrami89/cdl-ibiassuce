package br.com.micromais.cdlofertas.model;

public class Ofertas {
	private String title, thumbnailUrl;
	private String year, url;
	private String categoria;
	private String nomeloja, lojaThumbUrl;
	private String desconto;

	public Ofertas() {
	}

	public String getNomeloja() {
		return nomeloja;
	}

	public void setNomeloja(String nomeloja) {
		this.nomeloja = nomeloja;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String name) {
		this.title = name;
	}

	public String getThumbnailUrl() {
		return thumbnailUrl;
	}

	public void setThumbnailUrl(String thumbnailUrl) {
		this.thumbnailUrl = thumbnailUrl;
	}

	public String getYear() {
		return year;
	}

	public void setYear(String year) {
		this.year = year;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getCategoria() {
		return categoria;
	}

	public void setCategoria(String categoria) {
		this.categoria = categoria;
	}

	public String getLojaThumbUrl() {
		return lojaThumbUrl;
	}

	public void setLojaThumbUrl(String lojaThumbUrl) {
		this.lojaThumbUrl = lojaThumbUrl;
	}

	public String getDesconto() {
		return desconto;
	}

	public void setDesconto(String desconto) {
		this.desconto = desconto;
	}

	public Ofertas(String title, String thumbnailUrl, String year, String url, String categoria, String nomeloja, String lojaThumbUrl, String desconto) {
		this.title = title;
		this.thumbnailUrl = thumbnailUrl;
		this.year = year;
		this.url = url;
		this.categoria = categoria;
		this.nomeloja = nomeloja;
		this.lojaThumbUrl = lojaThumbUrl;
		this.desconto = desconto;
	}

}